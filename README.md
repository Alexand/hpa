# Horizontal Pod Autoscaler (HPA)

This is a study on how HPA works on K8s. **`NOT OFFICIAL DOCUMENTATION`**, although there are references to the official docs.

See also: [Kubernetes API Reference](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.13/#horizontalpodautoscalerspec-v1-autoscaling)

## Example

**!!! For the complete working example files copy from the repo, not from documentation !!!**

##### `Deployment`

```yaml
apiVersion: apps/v1 # for versions before 1.9.0 use apps/v1beta2
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  template:
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        resources:
          limits:
            cpu: 500m
          requests:
            cpu: 250m
        ...
```

##### `HorizontalPodAutoscaler`

```yaml
# hpa.yaml
apiVersion: autoscaling/v1
kind: HorizontalPodAutoscaler
metadata:
  name: hpa-nginx
spec:
  scaleTargetRef:
    apiVersion: apps/v1beta1
    kind: Deployment
    name: nginx-deployment
  minReplicas: 3
  maxReplicas: 5
  targetCPUUtilizationPercentage: 85
```

## Glossary

### {Deployment}spec.containers[].resources.requests.cpu

The minimum amount of available CPU that is requested for the container when provisioning the pod. In other words, a node must have at least this amount of CPU available so the pod can be provisioned in it. If there's not enough CPU, the pod will be provisioned somewhere else or wont be provisioned at all. See [How Pods with resource requests are scheduled](https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#how-pods-with-resource-requests-are-scheduled)

Besides, this value is also used to calculate the average of resource consumptions per pod.

>If a target utilization value is set, the controller calculates the utilization value as a percentage of the equivalent resource request on the containers in each pod. If a target raw value is set, the raw metric values are used directly. The controller then takes the mean of the utilization or the raw value (depending on the type of target specified) across all targeted pods, and produces a ratio used to scale the number of desired replicas. [(ref)](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/#how-does-the-horizontal-pod-autoscaler-work)

Getting even deeper in what this value represents, when using Docker:

`spec.containers[].resources.requests.cpu` is :
> converted to its core value, which is potentially fractional, and multiplied by 1024. The greater of this number or 2 is used as the value of the [--cpu-shares](https://docs.docker.com/engine/reference/run/#/cpu-share-constraint) flag in the docker run command. [(ref)](https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container#how-pods-with-resource-limits-are-run)

On a low level, this share is the weighed amount of time from a CPU cycle that will be given to this container when competing with other ones.

### {Deployment}spec.containers[].resources.limits.cpu

The maximum amount CPU that will be available for this container.

### {HorizontalPodAutoscaler}spec.targetCPUUtilizationPercentage

HPA starts scaling when the arithmetic mean of the CPU Utilization across all pods go above this value. It also has a 10% tolerance. So if this value is set to 80%, it will scale up when the the mean of CPU consumptions reaches 88%.

## [Autoscaling Algorithm] (https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/#algorithm-details)

In my own words:

1 - Collect the CPU utilization of every pod where:
  CPU utilization = average CPU usage over the last minute / resources.requests.cpu

2 - Calculate the arithmetic mean of this CPU utilization among all pods. Means: "How much an average pod is consuming"

3 - Calculate the number of pods that should be present:
desiredReplicas = ceil[currentReplicas * ( CPU utilization average (2) / target )]
